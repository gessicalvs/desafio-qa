# language: pt

Funcionalidade: Exibi��o de M�dia, Links e Docs de um grupo

CEN�RIO: #1 Localizar as op��es de visualiza��o de M�dia, Links e Docs de um grupo

DADO a sele��o do grupo "BATE PAPO QA",

QUANDO clicar no nome do grupo na parte superior da tela 

E selecionar a op��o "M�dia,Links e Docs"

ENT�O devem ser exibidos os bot�es "M�dia", "Links" e "Docs"

E em cada op��o seus respectivos tipos de arquivos enviados ao grupo "BATE PAPO QA"


CEN�RIO: #2 Exibir as m�dias recebidas num determinado grupo

DADO a sele��o do grupo "BATE PAPO QA",

QUANDO solicitar a exibi��o de m�dias

ENT�O devem ser exibidos apenas os arquivos de tipo "M�dia" (fotos,audios e videos)

E n�o devem ser exibidos outros tipos de arquivos


CEN�RIO: #3 Exibir os "Links" recebidos num determinado grupo

DADO a sele��o do grupo "BATE PAPO QA",

QUANDO solicitar a exibi��o de Links

ENT�O devem ser exibidos apenas os arquivos de tipo "Links" 

E n�o devem ser exibidos outros tipos de arquivos