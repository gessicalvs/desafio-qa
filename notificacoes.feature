
# language: pt

Funcionalidade: Notifica��es de mensagens

CEN�RIO: #1 Exibir notifica��es de grupo

DADO a op��o "Mostrar Notifica��es" na aba "Notifica��es de Grupo" localizada em Ajustes/Notifica��es

QUANDO habilitada a op��o "Mostrar Notifica��es"

ENT�O deve ser visualizada a op��o "Som"

E poder� ser selecionado a op��o de som desejada. 