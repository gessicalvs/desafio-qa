# language: pt

FUNCIONALIDADE: Enviar imagem para um contato

CEN�RIO: #1 Enviar imagem da biblioteca do celular

DADO que o usu�rio esteja na tela de conversa com algum contato

QUANDO selecionar a op��o "Fotos e V�deos"

ENTAO devem ser carregados as fotos e v�deos dispon�veis na biblioteca do smartphone

E deve ser selecionada uma imagem para envio


CEN�RIO: #2 Enviar imagem da biblioteca do celular

DADO que o usu�rio esteja na tela de conversa com algum contato

QUANDO selecionar a op��o "C�mera"

ENTAO deve ser aberta a c�mera do smartphone para capturar a imagem 

E depois de capturar a imagem deve ser exibido o campo pra inserir a legenda da foto

E o bot�o "Enviar" 